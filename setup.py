import setuptools

setuptools.setup(
    name="chem-vpn-test",
    version="0.0.1",
    author="Catherine Pitt",
    author_email="support@ch.cam.ac.uk",
    description="The Chemistry VPN test webapp",
    url="https://gitlab.developers.cam.ac.uk/ch/co/vpntest",
    packages=setuptools.find_packages(),
    install_requires=['flask','netaddr'],
    python_requires='>=3',
    include_package_data=True
)
