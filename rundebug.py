import os
import argparse

# Set debug before importing app so we get the logging right
os.environ['FLASK_DEBUG'] = '1'

# This isn't any use for testing the js parts of the app
p = argparse.ArgumentParser(description='Run VPN test app in test mode')
p.add_argument('ip',help='IP to fake',nargs='?',default='131.111.115.208')
args = p.parse_args()

from vpntest.vpntest import app
app.config['TEST_IP'] = args.ip
app.run()
