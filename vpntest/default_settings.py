"""
Our various VPN servers
"""
PREFERRED_SERVERS = ["openvpn2027.ch.cam.ac.uk"]
DEPRECATED_SERVERS = []
ALL_SERVERS = PREFERRED_SERVERS + DEPRECATED_SERVERS

CHEMNET_GATEWAY = "chemnet-gw2025.ch.cam.ac.uk"
