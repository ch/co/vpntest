$(document).ajaxStop(function() { try {
                                       if (ip_from_chem.ip == ip_from_outside.ip) {
                                           $("div.tunnel_type").html("You are using a full tunnel");
                                       } else {
                                          $("div.tunnel_type").html("You are using a split tunnel");
                                      }
                                  }
                                  catch(e) {
                                          $("div.tunnel_type").html("Could not determine tunnel type");
                                  }
                                 });

$.get("https://myip.ch.cam.ac.uk/?format=json",
      function(data) {
          ip_from_chem = data;
      }
);

$.get("https://api.ipify.org/?format=json",
      function(data) {
          ip_from_outside = data;
      }
);
