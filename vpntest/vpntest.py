'''
VPN test application
'''

from flask import Flask, request, render_template, send_from_directory, g
import os
import socket
from datetime import date
from netaddr import IPAddress, IPNetwork

app = Flask(__name__)
app.config.from_object('vpntest.default_settings')
app.config.from_pyfile('/etc/vpntest/servers.cfg',silent=True)
app.config.from_pyfile(os.path.expanduser('~/.config/vpntest/servers.cfg'),silent=True)

@app.before_request
def get_client_ip():
    test_ip = app.config.get('TEST_IP',None)
    if not test_ip:
        g.client_ip = request.environ.get('REMOTE_ADDR',None)
    else:
        g.client_ip = test_ip

@app.route('/')
def index():
    try:
        name = socket.gethostbyaddr(g.client_ip)[0]
    except (socket.gaierror,socket.herror) as e:
        name = None
    return render_template('test.html',
                           title='Chemistry VPN test',
                           ip=g.client_ip,
                           year=date.today().year,
                           name=name,
                           vpn_server_type=get_server_type(g.client_ip),
                           is_chemnet=is_chemnet(g.client_ip),
                           deprecated_servers = app.config['DEPRECATED_SERVERS']
                          )

def get_server_type(ipstr):
    '''
    Return a string describing the VPN server in use, given an IP address as a string
    '''
    vpn_server_type = 'unknown'
    ip = IPAddress(ipstr)
    chemistry_servers = [ IPAddress(socket.gethostbyname(x)) for x in app.config['ALL_SERVERS']]
    chemistry_preferred_servers = [ IPAddress(socket.gethostbyname(x)) for x in app.config['PREFERRED_SERVERS']]
    chemistry_deprecated_servers = [ IPAddress(socket.gethostbyname(x)) for x in app.config['DEPRECATED_SERVERS']]
    if ip in chemistry_servers:
        if ip in chemistry_preferred_servers:
            vpn_server_type = 'chemistry_preferred'
        elif ip in chemistry_deprecated_servers:
            vpn_server_type = 'chemistry_deprecated'
        else:
            vpn_server_type = 'chemistry'
    return vpn_server_type

def is_chemnet(ipstr):
    '''
    Return a boolean indicating if the user is connected to chemnet
    '''
    ip = IPAddress(ipstr)
    chemnet_gateway_hostname = app.config['CHEMNET_GATEWAY']
    chemnet_gateway_ip = IPAddress(socket.gethostbyname(chemnet_gateway_hostname))
    return ip == chemnet_gateway_ip


@app.route('/pl/<path:filename>')
def pl_static(filename):
    return send_from_directory(app.root_path + '/pl/', filename)

@app.route('/jquery/<path:filename>')
def jquery_static(filename):
    return send_from_directory(app.root_path + '/jquery/', filename)
