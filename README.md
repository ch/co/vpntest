# The VPN test webapp

## To set up a test/dev environment
 1. Clone the repo
 1. Run: `python3 rundebug.py 131.111.112.203`
	- The optional argument is the IP address of a hypothetical test client
 1. If you can open a browser on the machine running this code, just head to localhost:5000
 1. If you're running this code on a remote server/workstation, these are two ways to access the app: 
	- On your machine run `ssh -L5000:localhost:5000 $devcomputer`
	- Set up port forwarding via MobaSSHTunnel (MobaXTerm) - see below
 1. In your browser -> localhost:5000

(Note: 131.111.112.203 is the IP of the current openvpn2027 server)

### Port forwarding with MobaSSHTunnel
 1. Open MobaSSHTunnel (under Network in Tools tab)
 1. New SSH tunnel
	 - Select "Local port forwarding"
	 - \<Forwarded port\> = 5000
	 - \<SSH server\>     = exampleVM.ch.private.cam.ac.uk
	 - \<SSH login\>      = can be left empty
	 - \<SSH port\>       = 22
	 - \<Remote server\>  = localhost
	 - \<Remote port\>    = 5000
 1. Save
 1. Start the tunnel and you're ready 

## To release a new version
 1. commit all the changes to git and push
 1. run ./utils/do-release3 to tag the commit. The tag number will be generated automatically.
 1. run ./utils/package-vpntest which will build and release a package with a version matching the tag
 1. to just build but /not/ upload the package, use -d to the above script
